# 2. Semester Zusammenfassungen ITET

**Fehler in den Zusammenfassungen als Issue markieren (mit klarer Bezeichnung welche Zusammenfassung gemeint ist).**


## Formelsammlungen:
| Was?                                   | Wo?                                                            |
| :------------------------------------- | -------------------------------------------------------------- |
| Analysis 1&2 Formelsammlung (PDF)      | [hier](../AnalysisFormelsammlung/analysis.pdf)                 |
| Informatik 1 Formelsammlung (PDF)      | [hier](../Informatik1Formelsammlung/informatik.pdf)            |
| Komplexe Analysis Formelsammlung (PDF) | [hier](../KomplexeAnalysisFormelsammlung/komplexeanalysis.pdf) |
| Physik 1 Formelsammlung (PDF)          | [hier](../Physik1Formelsammlung/physik1.pdf)                   |


## Zusammenfassungen (alle unvollständig):
| Was?                                   | Wo?                                                            |
| :------------------------------------- | -------------------------------------------------------------- |
| Analysis 1&2 (PDF)                     | [hier](../Analysis/Analysis.pdf)                               |
| Informatik 1 (PDF)                     | [hier](../Informatik1/Informatik1.pdf)                         |
| Komplexe Analysis (PDF)                | [hier](../KomplexeAnalysis/KomplexeAnalysis.pdf)               |
| Netzwerke und Schaltungen 2 (PDF)      | [hier](../NuS2/NuS2.pdf)                                       |
| Physik 1 (PDF)                         | [hier](../Physik1/Physik1.pdf)                                 |

