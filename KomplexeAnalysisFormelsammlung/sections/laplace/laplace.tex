\begin{theorem}{Laplace}
    Die Laplace Transformation kann man sich eigentlich als Verallgemeinerung der Fourier Transformation vorstellen, diese konvergiert jedoch viel öfter! Die y-Achse der Laplace Transformation ist tatsächlich die Fourier Transformation. Anstatt eine periodisches Signal anzuschauen, betrachtet man das Signal nur vom Nullpunkt aus. Wie auch bei der Fourier Transformation wandelt die Laplace Transformation das Signal in den Frequenzbereich um.
\end{theorem}
\begin{iequation}[align*]
    &\mathcal{L}(x)(s) = \int_0^\infty x(t) e^{-st}dt \\
    & s = \sigma + i \omega
\end{iequation}

\subsection{Eigenschaften der Laplace Transformation}
    \begin{iequation}[align*]
        &\textnormal{1.  Linearität} \\
        &\textnormal{2.  } \mathcal{L}(f(t-a))(s) = e^{-as} \mathcal{L}(f)(s) \\
        &\textnormal{3.  } e^{at} \mathcal{L}(f) = \mathcal{L}(f)(s-a) \\
        &\textnormal{4.  } \mathcal{L}(f(at))(s) = \frac{1}{a} \mathcal{L}[f]\left( \frac{s}{a} \right) \\
        &\textnormal{5.  } \mathcal{L}(f')(s) = s \mathcal{L}(f)(s) - \lim_{t \to 0^+}f(t) \\
        &\textnormal{6.  } \frac{d^n}{ds^n} \mathcal{L}(f)(s) = (-1)^n \mathcal{L}(t^n f(t))(s) \\
        &\textnormal{7.  } \mathcal{L}(f*g)(s) = \mathcal{L}(f)(s) * \mathcal{L}(g)(s)
    \end{iequation}

\subsection{Rücktransformation}
    \begin{iequation}
        \begingroup
        \renewcommand{\arraystretch}{2} % Default value: 1
        \begin{array}{|c|c|}
            \hline
            f(t) & \mathcal{L}[f](s) \\
            \hline
            t & \frac{1}{s^2} \\
            \hline
            1 & \frac{1}{s}, \: s>0 \\
            \hline
            t^n & \frac{n!}{s^{n+1}}, \: s>0 \\
            \hline
            sin(at) & \frac{a}{s^2 + a^2}, \: s>0 \\
            \hline
            cos(at) & \frac{s}{s^2 + a^2}, \: s>0 \\
            \hline
            e^{at} & \frac{1}{s-a}, \: s>a \\
            \hline
            e^{at} * sin(bt) & \frac{b}{(s-a)^2 + b^2}, \: s>a \\
            \hline
            e^{at} * cos(bt) & \frac{s-a}{(s-a)^2 + b^2}, \: s>a \\
            \hline
            t^n e^{at} & \frac{n!}{(s-a)^{n+1}}, \: s>a \\
            \hline
            af(t) + bg(t) & a \mathcal{L}[f](s) + b \mathcal{L}[g](s) \\
            \hline
            tf(t) & - \frac{d}{ds} \left( \mathcal{L}[f](s) \right) \\
            \hline
            t^nf(t) & (-1)^n \frac{d^n}{ds^n} \left( \mathcal{L}[f](s) \right) \\
            \hline
            f'(t) & s \mathcal{L}[f](s) - f(0) \\
            \hline
            f''(t) & s^2 \mathcal{L}[f](s) - sf(0) - f'(0) \\
            \hline
            e^{at} * f(t) & \mathcal{L}[f](s-a) \\
            \hline
            \int_0^t f(\tau) g(t-\tau)d \tau & \mathcal{L}[f](s) * \mathcal{L}[g](s) \\
            \hline
        \end{array}
        \endgroup
    \end{iequation}

\subsection{Schema für Differentialgleichungen mit Laplace}
    \begin{enumerate}
        \item Beide Seiten transformieren $\Longrightarrow$ Eigenschaften anwenden
        \item Nach $y(s)$ auflösen
        \item Partialbruchzerlegung
        \item Mit Tabelle zurücktransformieren
    \end{enumerate}
    \begin{example}{Lösen einer Differentialgleichung mit Laplace}
        Löse folgende Gleichung mithilfe der Laplace Transformation:
        \begin{equation*}
            \ddot{y}(t) - 10 \dot{y}(t) + 9 y(t) = 5t \quad y(0) = 1 \quad \dot{y}(0) = 2
        \end{equation*}
        \begin{enumerate}
            \item Beide Seiten transformieren:
                \begin{align*}
                    & \ddot{y}(t) \laplace s^2 \cdot Y(s) - sf(0) - f'(0) \\
                    & 10 \dot{y}(t) \laplace 10s \cdot Y(s) - 10f(0) \\
                    & 9y(t) \laplace 9 Y(s) \\
                    & 5t \laplace \frac{5}{s^2} \\
                    & \Longrightarrow s^2 Y(s) + s - 2 - 10s Y(s) - 10 + 9 Y(s) = \frac{5}{s^2}
                \end{align*}
            \item Nach $Y(s)$ umwandeln:
                \begin{equation*}
                    Y(s) = \frac{-s^3 + 12 s^2 + 5}{s^2 (s-1)(s-9)}
                \end{equation*}
            \item Partialbruchzerlegung:
                \begin{align*}
                    Y(s) = \frac{-s^3 + 12 s^2 + 5}{s^2 (s-1)(s-9)} & = \frac{A}{s} + \frac{B}{s^2} + \frac{C}{(s-1)} + \frac{D}{(s-9)} \\
                    & = \frac{50}{81s} + \frac{5}{9s^2} + \frac{31}{81(s-9)} - \frac{2}{(s-1)}
                \end{align*}
            \item Zurücktransformieren:
                \begin{equation*}
                    y(t) = \frac{50}{81} + \frac{5}{9}t + \frac{31}{81}e^{9t} - 2 e^t
                \end{equation*}
        \end{enumerate}
    \end{example}

\subsection{Dirac Delta}
    \begin{theorem}{Dirac Delta}
        Die Dirac-Delta Funktion beschreibt eigentlich einen Nagelimpuls. Ein Unendlicher Impuls hat dementsprechend alle Frequenzen drin.
    \end{theorem}
    \begin{figure}[H]
        \includegraphics[width=6cm]{Bilder/DiracDelta.png}
        \centering
    \end{figure}
    3 Fakten über das Dirac Delta $\delta (t)$
    \begin{iequation}[align*]
        & \int_{- \infty}^\infty \delta (t) dt = 1 \\
        & \int_{- \infty}^\infty \delta (t-a) f(t) dt = f(a) \\
        & \mathcal{L}(\delta(t)) = e^{-as}
    \end{iequation}