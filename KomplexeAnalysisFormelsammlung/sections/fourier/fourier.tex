\subsection{Fourierreihen}
    \begin{theorem}{Fourierreihen}
        Stückweise stetige 2-L periodische Funktionen deren linke und rechte Ableitung überall existieren können wir nach dem Satz von Dirichlet als Fourierreihen entwickeln.
    \end{theorem}
    \begin{iequation}
        f(t) ~ \frac{a_0}{2} + \sum_{n=1}^\infty a_n cos(\overbrace{n \frac{\pi}{L}}^{\omega = \frac{2 \pi}{T}} t) + b_n sin(n \frac{\pi}{L} t)
    \end{iequation}
    in komplexer Schreibweise:
    \begin{iequation}
        f(t) ~ \sum_{n=-\infty}^\infty c_n e^{in \frac{\pi}{L} t}
    \end{iequation}
    Wobei $c_n$ komplex ist!
    \subsubsection{Konvergenz}
        \begin{figure}[H]
            \includegraphics[width=8cm]{Bilder/konvergent1.jpg}
            \centering
        \end{figure}
        \begin{figure}[H]
            \includegraphics[width=8cm]{Bilder/konvergent2.jpg}
            \centering
        \end{figure}
    \subsubsection{Koeffizienten}
        \begin{iequation}[align*]
            &a_n = \frac{1}{L} \int_{-L}^L f(t) cos(n \frac{\pi}{L} t) dt \\
            &b_n = \frac{1}{L} \int_{-L}^L f(t) sin(n \frac{\pi}{L} t) dt \\
            &c_n = \frac{1}{2L} \int_{-L}^L f(t) e^{-i n \frac{\pi}{L} t} dt 
        \end{iequation}
        \textbf{Für die Koeffizienten gilt:}\\
            $a_n = 0 \; \forall n$ falls $f(t)$ ungerade (Punktsymetrisch) \\
            $b_n = 0 \; \forall n$ falls $f(t)$ gerade (Spiegelung y-Achse)
    \subsubsection{Beziehung zwischen reeller und komplexer Reihe}
        \begin{iequation}[align*]
            &a_n = c_n + c_{-n} \\
            &b_n = i(c_n - c_{-n})
        \end{iequation}
        \begin{iequation}[align*]
            &c_n = \frac{1}{2} (a-ib) \\
            &c_{-n} = \frac{1}{2} (a+ib)
        \end{iequation}
    \subsubsection{Satz von Parseval}
        Für eine $2 \pi$ periodische Funktion gilt:
        \begin{iequation}
            \frac{1}{2 \pi} \int_{- \pi}^\pi \left\lvert f(t) \right\rvert^2 dt = \sum_{n=-\infty}^\infty \left\lvert c_n \right\rvert^2
        \end{iequation}
        oder in reeller Form:
        \begin{iequation}
            \frac{1}{2 \pi} \int_{-\pi}^\pi \left\lvert f(t) \right\rvert^2 dt = \frac{1}{2} \left( \frac{a_0^2}{2} + \sum_{n=1}^\infty a_n^2 + b_n^2 \right)
        \end{iequation}

\subsection{Fouriertransformation}
    \begin{theorem}{Fouriertransformation}
        Bei der Fouriertransformation wird das Signal in ein Spektralspektrum zerlegt.
    \end{theorem}
    \begin{iequation}
        \hat{f}(\omega) = \frac{1}{\sqrt{2 \pi}} \int_{- \infty}^\infty f(t) e^{-i \omega t} dt
    \end{iequation}
    \begin{figure}[H]
        \includegraphics[width=8cm]{Bilder/Fouriertransformation.png}
        \centering
    \end{figure}
    Die Fouriertransformation existiert nur für absolut integrierbare Funktionen, also wenn:
    \begin{equation*}
        \int_{- \infty}^\infty \left\lvert f(t) \right\rvert dt < \infty
    \end{equation*}
    \begin{example}{Absolute Integrierbarkeit}
        \begin{center}
            \begin{tabular}{c c}
                \textbf{absolut Integrierbar} & \textbf{nicht absolut Integrierbar} \\
                $e^{-x^2}$ & $sin(x)$ \\
                $\frac{1}{1+x^2}$ & $cos(x)$
            \end{tabular}
        \end{center}
    \end{example}
    \subsubsection{Eigenschaften der Fouriertransformation}
        \begin{iequation}[align*]
            \textnormal{1.  } &\textnormal{Linearität} \\
             &(\widehat{\alpha f + \beta g})(\omega) = \alpha \hat{f} (\omega) + \beta \hat{g}(\omega) \\
            \textnormal{2.  } &\widehat{f(t-a)}(\omega) = e^{-i \omega a} \hat{f}(\omega) \\
            \textnormal{3.  } &\widehat{e^{i a t} f(t)}(\omega) = \hat{f}(\omega - a) \\
            \textnormal{4.  } &\widehat{f(at)}(\omega) = \frac{1}{\left\lvert a \right\rvert} \hat{f}\left(\frac{\omega}{a}\right) \\
            \textnormal{5.  } &\hat{\hat{f}}(t) = f(-t) \\
            \textnormal{6.  } &\widehat{f^{(n)}}(\omega) = (i\omega)^n \hat{f}(\omega) \\
            \textnormal{7.  } &\widehat{t^n f(t)}(\omega) = i^n \frac{d^n}{d \omega^n} \hat{f}(\omega)
        \end{iequation}
        \textbf{Umkehrtransformation: } \\
        \begin{iequation}
            f(t) = \frac{1}{\sqrt{2 \pi}} \int_{- \infty}^\infty \hat{f}(\omega) e^{i \omega t} dt
        \end{iequation}
    \subsubsection{Satz von Planckerel}
        \begin{theorem}{Planckerel}
            Der Satz von Planckerel sagt aus, dass die Gesamtenergie eines Signals durch die Fouriertransformation erhalten bleibt. Sprich sowohl in der Frequenzdarstellung als auch in der Zeitdarstellung hat man denselben Wert für den Absolutbetrag!
        \end{theorem}
        \begin{iequation}
            \int_{-\infty}^\infty \left\lvert f(t) \right\rvert^2 dt = \int_{- \infty}^\infty \left\lvert \hat{f}(\omega) \right\rvert^2 d\omega
        \end{iequation}
        Gilt nur wenn $f(t)$ als auch $\hat{f}(\omega)$ absolut Integrierbar sind.

\subsection{Faltung}
    \begin{theorem}{Faltung}
        Die Faltung ist eine neue Operation, sie beschreibt die Überlagerungsfläche von 2 Funktionen.
    \end{theorem}
    \begin{figure}[H]
        \includegraphics[width=9cm]{Bilder/Faltung.png}
        \centering
    \end{figure}
    \subsubsection{Schema Faltung}
        \begin{enumerate}
            \item $f(t) \longrightarrow f(\tau)$ wobei $\tau$ eine ''Dummy-Variable'' ist
            \item $g(t) \longrightarrow g(-\tau)$ $g$ wird gespiegelt
            \item $t$ vergrössern entsprihct einer Verschiebung von $g$ nach rechts. Produkt $f(\tau) g(t-\tau)$ bilden und über ganz $\mathbb{R}$ integrieren
        \end{enumerate}
    \subsubsection{Eigenschaften der Faltung}
        ($f,g,h$ absolut integrierbar)
        \begin{iequation}[align*]
            \textnormal{1.  } &f*g = g*f \\
            \textnormal{2.  } &(f*g)*h = f*(g*h) \\
            \textnormal{3.  } &(\alpha f + \beta g)*h = \alpha f *h = \alpha f*h + \beta g*h \\
            \textnormal{4.  } &f(t-a)*g(t) = (f*g)(t-a) \\
            \textnormal{5.  } &\widehat{f*g}(\omega) = \sqrt{2 \pi} \hat{f}(\omega) \hat{g}(\omega) \\
             &F^{-1}(f*g) = \frac{1}{\sqrt{2 \pi}} F^{-1}(f) F^{-1}(g) \\
            \textnormal{6.  } &\widehat{(f*g)} = \sqrt{2 \pi} \hat{f} * \hat{g} \\
             &F^{-1}(f*g) = \frac{1}{\sqrt{2 \pi}} F^{-1}(f) * F^{-1}(g) 
        \end{iequation}