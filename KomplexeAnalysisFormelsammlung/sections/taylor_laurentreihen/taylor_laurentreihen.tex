\begin{theorem}{Laurentreihen}
    Die Laurentreihen kann man sich im Grunde als eine Erweiterung der Taylorreihen auf die komplexen Zahlen vorstellen. Dies entspricht einer Erweiterung um die negativen Exponenten im Polynom. Die Laurentreihe kann über eine auf einem Kreisring/Kreisscheibe holomorphe Funktion entwickelt werden. Innerhalb dieser Kreisscheibe konvergiert diese Laurentreihe (lässt sich über bekannte Reihen bestimmen).
\end{theorem}
\begin{figure}[H]
    \includegraphics[width=5cm]{Bilder/Laurent_series.png}
    \centering
\end{figure}
\begin{iequation}
    f(z) = \underbrace{\sum_{n=0}^\infty a_n(z-z_0)^n}_{\textnormal{positive Potenzen}} + \underbrace{\sum_{n=0}^\infty b_n \frac{1}{(z-z_0)^n}}_{\textnormal{negative Potenzen}}
\end{iequation}
oder in Kurzform:
\begin{iequation}
    f(z) = \sum_{n=-\infty}^\infty c_n (z-z_0)^n
\end{iequation}
Wichtig zu wissen ist, dass beliebig viele Komponenten Null sein dürfen ($a_n,b_n = 0$), es kann sich allenfalls auch um eine endliche Reihe handlen. \\
Oft berechnet man die Taylorreihe über bekannte Reihen, wie z.B. die geometrische Reihe.

\subsection{Allgemeines Vorgehen für Laurentreihen}
    \begin{figure}[H]
        \includegraphics[width=6cm]{Bilder/Laurentarten.jpg}
        \centering
    \end{figure}
    \begin{enumerate}
        \item $f(z)$ in Partialbrüche zerlegen
        \item Bestimmen auf welcher Menge wir die Reihe entwickeln wollen (in diesem Bereich darf es keine Singularitäten haben!)
        \item Abhängig davon welche Art von Laurentreihe wir entwickeln wollen (siehe Bild) in die passende Form bringen:
            \begingroup
            \renewcommand{\arraystretch}{2} % Default value: 1
            \begin{equation*}
            \begin{array}{l l}
                \textnormal{(1)} & f(z) = \frac{1}{1-h(z-z_0)} + \frac{1}{1-g(z-z_0)} \\
                \textnormal{(2)} & f(z) = \frac{1}{1- \widetilde{h}\left(\frac{1}{z-z_0}\right)} + \frac{1}{1-g(z-z_0)} \\
                \textnormal{(3)} & f(z) = \frac{1}{1 - \widetilde{h}\left(\frac{1}{z- z_0}\right)} + \frac{1}{1- \widetilde{g}\left(\frac{1}{z-z_0}\right)}
            \end{array}
            \end{equation*}
            \endgroup
        \item Mit Geometrischer Reihe entwickeln
        \item Überprüfen ob die Reihe wirlich konvergiert
    \end{enumerate}

\subsection{Singularitäten}
    \begin{theorem}{Singularitäten}
        Singularitäten beschreiben stellen, an welchen die Funktion nicht holomorph ist. Um jede noch so kleine Kresisscheibe um diesen Punkt muss es jedoch Punkte haben, die holomorph sind. Es handelt sich also um einen \dq Einzelfall\dq. Es gibt verschiedene Arten von Singularitäten, welche relativ einfach an den jeweiligen Laurentreihen erkannt werden können.
    \end{theorem}
    \begin{figure}[H]
        \includegraphics[width=8cm]{Bilder/Singularitaten.png}
        \centering
    \end{figure}
    \subsubsection{Isoliert}
        Eine isolierte Singularität bedeuted nichts mehr, als dass die Funktion in der Umgebung holomorph ist. Folgende Unterarten werden unterschieden: \\ \\
        \textbf{Hebbare Singularität:} \\
            Kann man durch hinzufügen einzelner Werte beheben (Lücken). \\
            $\Longrightarrow$ $c_n = 0$ für alle $n<0$, also alle negativen Koeffizienten gleich Null sind \\
        \textbf{Polstellen:} \\
            Funktionswerte gehen in der Nähe der Polstellen ins Unendliche. \\
            $\Longrightarrow$ $c_n = 0$ für alle Koeffizienten mit $n<-m$ also eine reelle Anzahl an negativen Exponenten \\
        \textbf{Wesentliche Singularität:} \\
            Trifft zu wenn es keine Hebbare aber auch keine Polstelle ist. \\
            $\Longrightarrow$ unendlich viele negative Koeffizienten ungleich Null sind \\ \\
        Überprüfen ob es sich tatsächlich um eine Polstelle und nicht um eine wesentliche Singularität handelt kann man mit folgendem Limes: 
        \begin{iequation}
            \lim_{z \to z_0} (z-z_0)^m f(z) \neq 0 \quad \textnormal{(und existiert)}
        \end{iequation}
        Wenn dieser Grenzwert existier und ungleich Null ist, handelt es sich um eine Polstelle.

\subsection{Residuensatz}
    \begin{theorem}{Residuensatz}
        Mit dem Residium berechnet man eigentlich einfach den Faktor $c_{-1}$ von der Laurentreihe. Der Residuensatz nutzt diese Residuen um zu berechnen wie sich das Wegintegral verhält wenn es eine Singularität umkreist. Dabei muss man nur das Residium der Singularität kennen und die Umlaufzahl (einfacher).
    \end{theorem}
    \begin{iequation}
        \int_\gamma f(z) dz = 2 \pi i \sum_{n=1}^N Res(f,z_k)
    \end{iequation}
    \subsubsection{Residuen berechnen}
        \begin{theorem}{Trick}
            Da das Residium einfach der Faktor $c_{-1}$ der Laurentreihe ist, kann man dieses einfach auslesen wenn man die Laurentreihe schon bestimmt hat.
        \end{theorem}
        \textbf{Residium berechnen bei einem Pol:}
            \begin{enumerate}
                \item Einen Pol m-ter Ordnung kann man schreiben als:
                    \begin{equation*}
                        f(z) = \frac{\phi(z)}{(z-z_0)^m} \qquad \textnormal{$\phi(z)$ holomorph}
                    \end{equation*}
                \item Dann kann man das Residium berechnen wie folgt:
                    \begin{equation*}
                        Res(f,Z_0) = \frac{\phi^{m-1}(z_0)}{(m-1)!} = \lim_{z \to z_0} \frac{d^{m-1}}{dz^{m-1}} (z-z_0)^m f(z)
                    \end{equation*}
            \end{enumerate}

\subsection{Uneigentliche Integrale}
    \begin{theorem}{Uneigentliche Integrale}
        Uneigentliche Integrale sind Integrale mit unbeschränktem Definitionsgebiet, gehen also gegen Unendlich!
    \end{theorem}
    Mit dem Residuensatz kann man gewisse uneigentliche Integrale der folgenden Form berechnen:
    \begin{iequation}
        \int_{-\infty}^\infty f(x) dx
    \end{iequation}
    Die Idee dabei ist, den reellen Raum ins komplexe zu erweitern und dann $f(z)$ über $\gamma$ zu integrieren (Wegintegral). \\
    \textbf{Nochmal anschauen....} \\
    Falls das Integral über den Halbkreis gegen null geht gilt:
    \begin{iequation}
        \int_{-\infty}^\infty f(x) dx = 2 \pi i \sum_{Im(z_k)>0} Res(f,z_k)
    \end{iequation}
    \begin{enumerate}
        \item \begin{equation*}
                f(z) := \frac{p(z)}{q(z)} h(z) \qquad \textnormal{mit $p(z)$, $q(z)$ Polynomen}
            \end{equation*}
        \item \begin{equation*}
                deg(q) \geqslant deq(p) + 2
            \end{equation*}
        \item \begin{equation*}
                \left\lvert h(z) \right\rvert < C \quad \textnormal{auf } \{z|Im(z) > 0 \}
            \end{equation*}
        \item \begin{equation*}
                q(z) \textnormal{ nicht null auf Reeller Achse}
            \end{equation*}
    \end{enumerate}
    Unter diesen Bedingungen ist $\lim_{R \to \infty} \int_{\gamma_2} f(z) dz = 0$