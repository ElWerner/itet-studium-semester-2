\subsection{Logik}
    \begin{center}
        \begin{tabular}{| c c | c c |}
            \hline
            $\lnot$ & Nicht & $\lor$ & Oder \\
            $\land$ & Und & $\forall$ & für Alle \\
            $\exists$ & Minimum 1 existiert & $\exists !$ & Genau 1 existiert \\
            $: |$ & sodass, gilt & $\in$ & Element von \\
            $\rightarrow$ & Implikation & $\Rightarrow$ & wahre Implikation \\
            \hline
        \end{tabular}
    \end{center}

    \subsubsection{Negation}
        \begin{iequation}[align*]
            &\lnot (A \land B) = \lnot A \lor \lnot B \\
            &\lnot (A \lor B) = \lnot A \land \lnot B \\
            &\lnot (\forall x, A(x)) = \exists x, \lnot A(x) \\
            &\lnot (\exists x, A(x)) = \forall x, \lnot A(x) \\
            &\lnot (A \longrightarrow B) = A \land \lnot B
        \end{iequation}


\subsection{Mengen}
    \begin{theorem}{Mengen}
        Mengen sind ungeordnete Zusammenfassungen verschiedener Objekte (sogenannter Elemente) zu einem Ganzen. Keine Elemente können mehrmals vorkommen!
    \end{theorem}
    \begin{theorem}{Teilmengen}
        Eine Teilmenge $I \subset \mathbb{R}$ ist ein Intervall $\Longleftrightarrow$ "$I$ hat keine Lücke"
    \end{theorem}

    \subsubsection{Zahlenmengen}
        \begin{flushleft}
            \begin{tabular}{l l}
                Natürliche Zahlen & $\mathbb{N} = \{1,2,3,\ldots\}$ \\
                Natürliche Zahlen mit 0 & $\mathbb{N}_0 = \{0,1,2,\ldots\}$ \\
                Ganze Zahlen & $\mathbb{Z} = \{-2,-1,0,1,2,\ldots\}$ \\
                Rationale Zahlen & $\mathbb{Q} = \{-3 \frac{4}{7},0,\frac{1}{2},3,\ldots\}$ \\
                Reelle Zahlen & $\mathbb{R} = \{\pi,e,\sqrt{2},\ldots\}$ \\
                Komplexe Zahlen & $\mathbb{C} = \{\imath,4-5\imath,-2\imath,e^\imath,\ldots\}$ \\
            \end{tabular}
        \end{flushleft}

    \subsubsection{Mengenoperationen}
        \begin{center}
            \begin{tabular}{| c c |}
                \hline
                $A \cup B$ & Vereinigung \\ 
                $A \cap B$ & Schnitt \\
                $A \times B$ & geordnete Paare \\
                $A \backslash B$ & Differenz \\
                $A \subset B$ & Teilmenge \\
                $\emptyset$ & leere Menge \\
                $card(A)$ & Anzahl Elemente in A \\
                $[a,b]$ & Abgeschlossen (inkl.) \\
                $(a,b)$ & Offen (exkl.) \\
                $X \backslash A =: A^c$ & Komplement von A \\
                \hline
            \end{tabular}
        \end{center}
        \begin{figure}[H]
            \includegraphics[width=4cm]{Bilder/Mengenoperationen.png}
            \centering
        \end{figure}
    
    \subsubsection{Untere und obere Schranken}
        \begin{theorem}{Schranken}
            Schranken sind grösser bzw. kleiner als alle Elemente in der Menge. Es gibt auch Mengen die keine obere/untere Schranken haben, wie z.B. die Reelle Zahlenmenge.
        \end{theorem}

    \subsubsection{Supremum und Infimum}
        \begin{theorem}{Supremum/Infimum}
            Das Supremum/Infimum stellen die kleinsten möglichen Schranken der Mengen dar. Wenn die Menge ein maximum/minimum hat, ist dieses das Supremum/Infimum. Man kann aber auch für offene Mengen ein Supremum/Infimum finden weil es sich auf die Schranken bezieht.
        \end{theorem}
        \begin{iequation}[align*]
        sup(X) = \textnormal{kleinste obere Schranke} \\
        inf(X) = \textnormal{grösste untere Schranke}
        \end{iequation}
        \begin{figure}[H]
            \includegraphics[width=8cm]{Bilder/Schranken.png}
            \centering
        \end{figure}

\subsection{Induktionsbeweis}
    Der Induktionsbeweis besteht im Grunde aus 3 Schritten:
    \begin{enumerate}
        \item \textbf{Induktionsverankerung} - Beweis mit bestimmtem Wert
        \item \textbf{Induktionsannahme} - Nimm an, dass die Aussage für n gilt
        \item \textbf{Induktionsschnitt} - Beweise, dass die Aussage auch für $n+1$ gilt
    \end{enumerate}
    \begin{example}{Induktionsbeweis}
        \begin{equation*}
            1^2 + 2^2 + \ldots + n^2 = \frac{1}{6} n (n+1)(2n+1) \forall n \in \mathbb{N}
        \end{equation*}
        \begin{align*}
            \textnormal{1.\quad} & \frac{1}{6} 1 (1+1) (2*1+1) = 1 \\
            \textnormal{2.\quad} & \textnormal{Annahme stimmt für n} \\
            \textnormal{3.\quad} & \frac{1}{6} n (n+1) (2n+1) + (n+1)^2 \\
               & = (n+1) (\frac{1}{6} n (2n+1)+(n+1)) \\
               & = \frac{1}{6} (n+1) (n(2n+1) + 6n+6) \\
               & = \frac{1}{6} (n+1) (2n^2 + 7n + 6) \\
               & = \frac{1}{6} (n+1) (n+2) (2n+3) \\
               & = \frac{1}{6} (n+1) ((n+1)+1) (2(n+1)+1)
        \end{align*}
    \end{example}

\subsection{Abbildungen}
    \subsubsection{Surjektiv}
        Jeder Wert der Zielmenge wird \textbf{mindestens einmal} als Funktionswert angenommen. \\
        \textit{Gibt es für alle $y \in Y$ mindestens eine Lösung x der Gleichung $f(x)=y$?}
        \begin{figure}[H]
            \includegraphics[width=2cm]{Bilder/Surjektiv.png}
            \centering
        \end{figure}
    \subsubsection{Injektiv}
        Jeder Wert der Zielmenge wird \textbf{höchstens einmal} als Funktionswert angenommen. \\
        \textit{Gibt es für alle $y \in Y$ höchstens eine Lösung x der Gleichung $f(x)=y$?}
        \begin{figure}[H]
            \includegraphics[width=2cm]{Bilder/Injektiv.png}
            \centering
        \end{figure}
    \subsubsection{Bijektiv}
        Die Abbildung ist sowohl Surjektiv als auch Injektiv, d.h. jeder Wert der Zielmenge wird \textbf{genau einmal} als Funktionswert angenommen. Nur Bijektive Funktionen haben eine Umkehrfunktion! \\
        \textit{Gibt es für alle $y \in Y$ genau eine Lösung x der Gleichung $f(x)=y$?}
        \begin{figure}[H]
            \includegraphics[width=2cm]{Bilder/Bijektiv.png}
            \centering
        \end{figure}

\subsection{Binomialkoeffizient}
    Die Anzahl der Teilmengen der Menge $\{1,\ldots,n\}$ die k Elemente enthalten.
    \begin{iequation}[align*]
        \binom{n}{k} = \frac{n!}{k! (n-k)!}
    \end{iequation}
    \begin{figure}[H]
        \includegraphics[width=7cm]{Bilder/Pascalschesdreieck.png}
        \centering
    \end{figure}

\subsection{Absolutbetrag}
    Ist einfach das positive der Zahl\dots

\subsection{Euklidische Norm}
    Ist die Länge des Vektors\dots
    \begin{iequation}[align*]
        \left\lvert \left\lvert x \right\rvert \right\rvert := \sqrt{x * x} = \sqrt{\sum_{i=1}^n x_i^2}
    \end{iequation}

\subsection{Mitternachtsformel}
    \begin{iequation}
        x_{1,2} = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}
    \end{iequation}

\subsection{Geometrie}
    \subsubsection{Allgemein}
        \textbf{Kreis:}
        \begin{iequation}[align*]
            &A = \pi r^2 \\
            &U = 2 \pi r
        \end{iequation}
        \textbf{Kugel:}
        \begin{iequation}[align*]
            &V = \frac{4}{3} \pi r^3 \\
            &A = 4 \pi r^2
        \end{iequation}
        \textbf{Wichtig:}
        \begin{iequation}
            \cos^2(x) + \sin^2(x) = 1
        \end{iequation}

    \subsubsection{Trigonometrische Funktionen}
        \begin{center}
            \begin{tabular}{|c|c|c|c|c|c|}
                \hline
                & 0° & 30° & 45° & 60° & 90° \\
                \hline
                $\sin(x)$ & $0$ & $\frac{1}{2}$ & $\frac{\sqrt{2}}{2}$ & $\frac{\sqrt{3}}{2}$ & $1$ \\
                $ \cos(x)$ & $1$ & $\frac{\sqrt{3}}{2}$ & $\frac{\sqrt{2}}{2}$ & $\frac{1}{2}$ & $0$ \\
                $tan(x)$ & $0$ & $\frac{\sqrt{3}}{3}$ & $1$ & $\sqrt{3}$ & $\pm \infty$ \\
                \hline
            \end{tabular}
        \end{center}
        \begin{figure}[H]
            \includegraphics[width=6cm]{Bilder/Einheitskreis.png}
            \centering
        \end{figure}
    
    \subsubsection{Kosinussatz}
        \begin{iequation}
            c^2 = a^2 + b^2 - 2ab \cdot \cos(\gamma)
        \end{iequation}

    \subsubsection{Sinussatz}
        \begin{iequation}
            \frac{a}{\sin(\alpha)} = \frac{b}{\sin(\beta)} = \frac{c}{\sin(\gamma)} = 2r
        \end{iequation}

    \subsubsection{Additionstheoreme}
        \begin{iequation}[align*]
            & \cos(x \pm y) = \cos(x) \cos(y) \mp \sin(x) \sin(y) \\
            & \sin(x \pm y) = \sin(x) \cos(y) \pm \cos(x) \sin(y) \\
            & \tan(x \pm y) = \frac{tan(x) \pm \tan(y)}{1 \mp \tan(x) \tan(y)}
        \end{iequation}

    \subsubsection{Wichtige Zusammenhänge}
        \begin{iequation}
        \begingroup
        \renewcommand{\arraystretch}{2} % Default value: 1
        \begin{array}{l}
            1 = \cos^2(x) + \sin^2(x) \\
            \sin^2(x) = \frac{1}{2} (1 - \cos(2x)) \\
            \cos^2(x) = \frac{1}{2} (1 + \cos(2x)) \\
            \sin(2x) = 2 \cdot \sin(x) \cdot \cos(x) \\
            \cos(2x) = \cos^2(x) - \sin^2(x) \\
            \sin(\arccos(x)) = \sqrt{1 - x^2} \\
            \cos(\arcsin(x)) = \sqrt{1 - x^2} \\
            \sin^3(x) = \frac{1}{4}(3 \sin(x) - \sin(3x)) \\
            \cos^3(x) = \frac{1}{4} (3\cos(x) + \cos(3x)) \\
            \hline
            1 = \cosh^2(x) - \sinh^2(x) \\
            \sinh^2(x) = \frac{1}{2} (\cosh(2x) - 1) \\
            \cosh^2(x) = \frac{1}{2} (\cosh(2x) + 1) \\
            \sinh(2x) = 2 \sinh(x) \cosh(x) \\
            \cosh(2x) =  \cosh^2(x) + \sinh^2(x) \\
            \sinh(\arcosh(x)) = \sqrt{x^2 - 1} \\
            \cosh(\arsinh(x)) = \sqrt{x^2 + 1}
        \end{array}
        \endgroup
        \end{iequation}
    
    \subsubsection{Kugelgleichung}
        \begin{iequation}
            x^2 + y^2 + z^2 = r^2
        \end{iequation}

\subsection{Dreiecks-Ungleichung}
    \begin{iequation}[align*]
        \left\lvert x + y \right\rvert \leqslant \left\lvert x \right\rvert + \left\lvert y \right\rvert
    \end{iequation}
    \begin{iequation}
        \left\lvert \sum_{i=1}^n x_i \right\rvert \leq \sum_{i=1}^n \left\lvert x_i \right\rvert
    \end{iequation}
    \begin{iequation}
        \left\lvert \int_I f(x) dx \right\rvert \leq \int_I \left\lvert f(x) \right\rvert dx
    \end{iequation}

\subsection{Young-Ungleichung}
    Gilt für $x,y \in \mathbb{R}$ und $\epsilon > 0$
    \begin{iequation}[align*]
        2 \left\lvert x*y \right\rvert \leqslant \epsilon x^2 + \frac{1}{\epsilon} y^2
    \end{iequation}

\subsection{Cauchy-Schwartz-Ungleichung}
    Für alle $x,y \in \mathbb{R}^n$
    \begin{iequation}[align*]
        \left\lvert x*y \right\rvert \leqslant \left\lvert \left\lvert x\right\rvert \right\rvert * \left\lvert \left\lvert y \right\rvert \right\rvert 
    \end{iequation}

\subsection{Partialbruchzerlegung}
    \begin{iequation}
        \frac{P_n(x)}{Q_m(x)}
    \end{iequation}
    \begin{enumerate}
        \item Polynomdivision (falls $n>m$) mit Rest
        \item Nullstellen von $Q_m(x)$ berechnen
        \item Nullstellen ihrem Partialbruch zuordnen:
        \begin{itemize}
            \item reelle r-fache Nullstelle $x_0$:
            \begin{equation*}
                \frac{A_1}{(x-x_0)} + \frac{A_2}{(x-x_0)^2} + \dots + \frac{A_r}{(x-x_0)^r}
            \end{equation*}
            \item komplexe r-fache Nullstelle:
            \begin{equation*}
                \frac{A_1x+B_1}{(x^2+2ax+b)} + \frac{A_2x+B_2}{(x^2+2ax+b)^2} + \dots + \frac{A_rx+B_r}{(x^2+2ax+b)^r}
            \end{equation*}
        \end{itemize}
        \item Gleichung aufstellen
    \end{enumerate}
    \begin{example}{Partialbruchzerlegung}
        Man soll die Partialbruchzerlegung an folgender Funktion durchführen:
        \begin{equation*}
            \frac{x+10}{x^2 + 5x - 14}
        \end{equation*}
        \begin{enumerate}
            \item Nicht notwendig, da n<m
            \item Nullstellen berechnen:
                \begin{align*}
                    & x^2 + 5x - 14 = (x+7)(x-2) \\
                    & \Longrightarrow \frac{x+10}{(x+7)(x-2)}
                \end{align*}
            \item Nullstelen ihrem Partialbruch zuordnen:
                \begin{equation*}
                    \frac{x+10}{(x+7)(x-2)} = \frac{A}{(x+7)} + \frac{B}{(x-2)}
                \end{equation*}
            \item Auflösen:
                \begin{align*}
                    & \frac{A}{(x+7)} + \frac{B}{(x-2)} = \frac{A(x-2) + B(x+7}{(x+7(x-2))} = \frac{Ax -2A + Bx + 7B}{(x+7(x-2))} \\
                    & 1 = A + B \\
                    & 10 = -2A + 7B \\
                    & \Longrightarrow A = - \frac{1}{3} \quad B = \frac{4}{3}
                \end{align*}
        \end{enumerate}
    \end{example}

\subsection{Polynomdivision}
    \begin{example}{Polynomdivision}
        \begin{align*}
            &(-2x^2 - x -1) : (x-1) = -2x -3 + \frac{-4}{x-1} \\
            &\underline{2x^2 - 2x} \\
            &\qquad -3x-1 \\
            &\qquad \underline{3x-3} \\
            &\qquad \qquad -4
        \end{align*}
    \end{example}